package com.smok.agnieszka.attendance.models;

import android.text.Layout;

/**
 * Created by mazg on 12.12.2017.
 */

public class Participant {
    private String name;
    private String surrname;
    private boolean isPaied;
    private boolean isAttendend;

    public Participant(String name, String surrname) {
        this.name = name;
        this.surrname = surrname;
    }

    public boolean isPaied() {
        return isPaied;
    }

    public void setPaied(boolean paied) {
        isPaied = paied;
    }

    public boolean isAttendend() {
        return isAttendend;
    }

    public void setAttendend(boolean attendend) {
        isAttendend = attendend;
    }
}
