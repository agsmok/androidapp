package com.smok.agnieszka.attendance.models;

import java.util.ArrayList;

/**
 * Created by mazg on 12.12.2017.
 */

public class Class {


    private final String name;
    private final ArrayList<Participant> participants;

    public Class(String name, ArrayList<Participant>participants) {
        this.name = name;
        this.participants = participants;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Participant> getParticipants() {
        return participants;
    }
}
