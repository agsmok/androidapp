package com.smok.agnieszka.attendance.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.smok.agnieszka.attendance.R;
import com.smok.agnieszka.attendance.adapters.ClassesAdapter;
import com.smok.agnieszka.attendance.adapters.ParticipantsAdapter;
import com.smok.agnieszka.attendance.models.Class;
import com.smok.agnieszka.attendance.models.Participant;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Class> listOfClasses = new ArrayList<>();
    ArrayList<Participant> listOfParticipant = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ClassesAdapter classesAdapter = new ClassesAdapter(this, listOfClasses);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(classesAdapter);

        Class aClass = new Class("Krasnoludki", new ArrayList<Participant>());
        Class aClass1 = new Class("Aerobik", new ArrayList<Participant>());
        Class aClass2 = new Class("Akademia wyobraźni", new ArrayList<Participant>());
        Class aClasa3 = new Class("Zsjecia muzyczne", new ArrayList<Participant>());

        addClasses(aClass, aClass1, aClass2, aClasa3);
        listView.invalidate();
    }

    void addClasses(Class first, Class... classes) {
        System.out.println("liczba dodanych osób: " + (classes.length + 1));
        listOfClasses.add(first);
        for (int i = 0; i < classes.length; i++) {
            listOfClasses.add(classes[i]);
        }
    }
}



