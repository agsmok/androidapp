package com.smok.agnieszka.attendance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smok.agnieszka.attendance.R;
import com.smok.agnieszka.attendance.models.Class;

import java.util.ArrayList;

/**
 * Created by mazg on 12.12.2017.
 */

public class ClassesAdapter extends BaseAdapter {

    private final Context context;
    private final ArrayList<Class> classes;

    public ClassesAdapter(Context context, ArrayList<Class> classes) {
        this.context = context;
        this.classes = classes;
    }

    @Override
    public int getCount() {
        return classes.size();
    }

    @Override
    public Object getItem(int position) {
        return classes.get(position);
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.class_item, null);
            holder = new ViewHolder();
            holder.className = view.findViewById(R.id.classNameHolder);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Class object = classes.get(position);
        holder.className.setText(object.getName());
        return view;
    }

    private static class ViewHolder {
        TextView className;

    }

}
